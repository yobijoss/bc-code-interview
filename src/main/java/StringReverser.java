/*
 * Copyright 2021, Charter Communications, All rights reserved.
 */

import java.util.Arrays;

public class StringReverser {


  public static String reverseString(String text) {

    char[] array = text.toCharArray();
    final int middle = (text.length() / 2) - 1;

    for (int i = 0; i <= middle; i++) {

      final int tail = (text.length() - 1) - i;

      char buffer = array[i];
      array[i] = array[tail];
      array[tail] = buffer;

    }

    return new String(array);
  }
}
