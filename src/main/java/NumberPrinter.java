/*
 * Copyright 2021, Charter Communications, All rights reserved.
 */

public class NumberPrinter {

  public int[] printPrimeNumbers(int numberToPrint) {
    final int[] primeNumberArray = new int[numberToPrint];

    int counter = 0;
    int i = 1;

    while (counter < numberToPrint) {
      i++;

      if (isPrime(i)) {
        primeNumberArray[counter++] = i;
      }
    }


    return primeNumberArray;
  }

  private boolean isPrime(int number) {
    boolean isPrime = true;
    for (int i = number - 1; i > 1 ; i--) {
      if (number % i == 0) {
        isPrime = false;
        break;
      }
    }
    return isPrime;
  }
}
