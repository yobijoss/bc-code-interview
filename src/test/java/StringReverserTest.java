import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StringReverserTest {

  @Test
  public void testStringReversed() {

    String normalOdd = "jose angel";
    final String reverseOddString = StringReverser.reverseString(normalOdd);
    assertEquals("legna esoj", reverseOddString);

    String normalPair = "ferraris";
    final String reversePairString = StringReverser.reverseString(normalPair);
    assertEquals("sirarref", reversePairString);

    final String empty = StringReverser.reverseString("");
    assertEquals("", empty);


    final String unit = StringReverser.reverseString("a");
    assertEquals("a", unit);


  }
}
