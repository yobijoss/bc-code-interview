import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class NumberPrinterTest {

  @Test
  void printFirst100Number() {

    final int[] ints = new NumberPrinter().printPrimeNumbers(15);

    assertEquals(2, ints[0]);
    assertEquals(3, ints[1]);
    assertEquals(5, ints[2]);
    assertEquals(7, ints[3]);
    assertEquals(11, ints[4]);
    assertEquals(13, ints[5]);
    assertEquals(17, ints[6]);
    assertEquals(19, ints[7]);
    assertEquals(23, ints[8]);
    assertEquals(29, ints[9]);
    assertEquals(31, ints[10]);
    assertEquals(37, ints[11]);
    assertEquals(41, ints[12]);
    assertEquals(43, ints[13]);
    assertEquals(47, ints[14]);

  }

  @Test
  void printFirst5PrimeNumber() {
    final int[] ints = new NumberPrinter().printPrimeNumbers(5);

    assertEquals(2, ints[0]);
    assertEquals(3, ints[1]);
    assertEquals(5, ints[2]);
    assertEquals(7, ints[3]);
    assertEquals(11, ints[4]);
  }
}
